from flask import Flask, jsonify
import os

app = Flask(__name__)

username = os.environ.get("USERNAME")
password = os.environ.get("PASSWORD")
cluster_url = os.environ.get("CLUSTER_URL")


@app.route("/homepage")
def homepage():
    return "Welcome <YOUR_FULL_NAME>! You are authenticated to use the API.".replace(
        "<YOUR_FULL_NAME>", "Ascol Parajuli"
    )

@app.route("/mongodb")
def mongobdcon():
    json_new_respose = {
        "CLUSTER_URL": f"mongodb://{username}:{password}@{cluster_url}",
        "DATA": {"age": 50, "height": 182, "Weight": 85},
    }
    return jsonify(json_new_respose)
  

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)