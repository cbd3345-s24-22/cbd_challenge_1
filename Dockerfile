# base image for container
FROM python

ENV PYTHONUNBUFFERED 1

# set default working dir
WORKDIR /app

#copy 
COPY . .

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

CMD ["python","app.py"]




